import UIKit

// Замыкание 1
let closure = {
    print("Hello, World !!!")
}

func repeatThereeTimes(closure: () -> ()){
    for _ in 0...2 {
        closure()
    }
}

repeatThereeTimes(closure: closure)

// Замыкание 2
func repeatThereeTimes2(closure: () -> ()){
    for _ in 0...2 {
        closure()
    }
}

repeatThereeTimes2 {
    () -> () in
    print("Hello, World !!!")
}

//замыкание 3 посложней
let unsortedArray = [123, 2, 32, 67, 8797, 432]
let sortedArray = unsortedArray.sorted {
    (number1: Int, number2: Int) -> Bool in
    return number1 < number2
}
