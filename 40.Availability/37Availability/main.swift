//  SwiftBook.ru
//  Availability check (проверка доступности версии ОС)

func anyOSMethod(){
  print("Works on every OS")
}

@available(iOS 8, OSX 10.10, *)
func iOS8Method() {
  print("Works only on iOS 8 and higher")
}

@available(iOS 9, OSX 10.11, watchOS 2, *)
func iOS9Method() {
  print("Works only on iOS 9 and higher")
}

func useApropriateMethods() {
  anyOSMethod()
  
  if #available(iOS 8, OSX 10.10, *) {
      iOS8Method()
  } else {
      anyOSMethod()
  }
  
  if #available(iOS 9, OSX 10.11, watchOS 2, *) {
      iOS9Method()
  }
  
  guard #available(iOS 9, OSX 10.11, watchOS 2, *) else { return }
  iOS9Method()
  
}